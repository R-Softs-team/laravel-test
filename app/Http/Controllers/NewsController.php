<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function store(int $companyId, Request $request)
    {
        $request->offsetSet('company_id',  $companyId);

        $request->validate([
            'company_id' => 'required|exists:companies,id'
        ]);

        $rand = rand(0, 200);
        $news = new News();

        $news->company_id = $companyId;
        $news->title = 'новость '.$rand;
        $news->content = 'сожержание новости '.$rand;
        $news->save();

        return redirect('/home');
    }
}
