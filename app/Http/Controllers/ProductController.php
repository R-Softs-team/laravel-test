<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @param int $companyId
     * @param Request $request
     */
    public function store(int $companyId, Request $request)
    {
        $request->offsetSet('company_id',  $companyId);

        $request->validate([
            'company_id' => 'required|exists:companies,id'
        ]);

        $rand = rand(0, 200);
        $product = new Product();

        $product->company_id = $companyId;
        $product->name = 'продукт '.$rand;
        $product->price = round($rand, 2);
        $product->save();

        return redirect('/home');
    }
}
