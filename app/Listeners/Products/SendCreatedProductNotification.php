<?php

namespace App\Listeners\Products;

use App\Events\Products\CreatedProductEvent;
use App\Models\Product;
use App\Models\User;
use App\Notifications\CreatedProductNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCreatedProductNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedProductEvent  $event
     * @return void
     */
    public function handle(CreatedProductEvent $event)
    {
        $subscribers = User::subscribed(CreatedProductEvent::class)->get();

        \Notification::send($subscribers, new CreatedProductNotification($event->getProduct()));
    }
}
