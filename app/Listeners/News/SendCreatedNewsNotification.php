<?php

namespace App\Listeners\News;

use App\Events\News\CreatedNewsEvent;
use App\Events\Products\CreatedProductEvent;
use App\Models\User;
use App\Notifications\CreatedNewsNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCreatedNewsNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedNewsEvent  $event
     * @return void
     */
    public function handle(CreatedNewsEvent $event)
    {
        $subscribers = User::subscribed(CreatedNewsEvent::class)->get();

        \Notification::send($subscribers, new CreatedNewsNotification(($event->getNews())));
    }
}
