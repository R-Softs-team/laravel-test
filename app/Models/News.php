<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use HasFactory;
    use Timestamp;

    protected $fillable = [
        'company_id', 'title', 'content'
    ];

    /** Relations */

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }
}
