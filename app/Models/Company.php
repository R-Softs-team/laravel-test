<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    use Timestamp;

    protected $fillable = [
        'name'
    ];

    /** Relations */

    public function employees()
    {
        return $this->hasMany(\App\Models\User::class);
    }

    public function news()
    {
        return $this->hasMany(\App\Models\News::class);
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }


}
