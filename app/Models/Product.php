<?php

namespace App\Models;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use Timestamp;

    protected $fillable = [
        'company_id', 'name', 'price'
    ];

    /** Relations */

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }
}
