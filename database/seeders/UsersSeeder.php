<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = date('Y-m-d H:i:s');

        DB::table('users')->insert([
            ['id' => null, 'company_id'=>1, 'name' => 'user1', 'email' => 'user1@example.com' ,'email_verified_at'=>$timestamp, 'password' => bcrypt('user1'), 'created_at'=>$timestamp],
            ['id' => null, 'company_id'=>1, 'name' => 'user2', 'email' => 'user2@example.com' ,'email_verified_at'=>$timestamp, 'password' => bcrypt('user2'), 'created_at'=>$timestamp],
            ['id' => null, 'company_id'=>2, 'name' => 'user3', 'email' => 'user3@example.com' ,'email_verified_at'=>$timestamp, 'password' => bcrypt('user3'), 'created_at'=>$timestamp],
            ['id' => null, 'company_id'=>null, 'name' => 'user4', 'email' => 'user4@example.com' ,'email_verified_at'=>$timestamp, 'password' => bcrypt('user4'), 'created_at'=>$timestamp],
        ]);

    }
}
