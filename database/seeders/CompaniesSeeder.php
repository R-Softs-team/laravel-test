<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = date('Y-m-d H:i:s');

        DB::table('companies')->insert([
            ['name' => 'company1', 'created_at' => $timestamp],
            ['name' => 'company2', 'created_at' => $timestamp],
        ]);
    }
}
