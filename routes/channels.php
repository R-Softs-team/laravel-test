<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.News.{id}', function ($news, $id) {
    return (int) $news->id === (int) $id;
});

Broadcast::channel('App.Models.Product.{id}', function ($product, $id) {
    return (int) $product->id === (int) $id;
});
